# Phone Number Lookup & Verification API - [numlookupapi.com](https://numlookupapi.com)

Automate your phone number verification process by checking carrier information with our free and worldwide phone number lookup API.

## Your first request

Get your free API key [here](https://app.numlookupapi.com/register) and simply query our /validate endpoint:

```
# check a phone number
curl "https://api.numlookupapi.com/v1/validate/+14158586273?apikey=YOUR-APIKEY"

```

## Pricing

Our packages start at 9.99$/month for 7,000 API requests, you can find more here: [Pricing](https://numlookupapi.com/pricing/)

## Endpoints

Check out all the available endpoints here: [https://numlookupapi.com/docs/status](https://numlookupapi.com/docs/status)

## Support And Contact

Please send us an email at [support@numlookupapi.com](mailto:support@numlookupapi.com)
